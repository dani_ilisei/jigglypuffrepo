//
//  Utils.h
//  GuessMe
//
//  Created by Daniel Ilisei on 11/21/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kLabelWidth 100
#define kButtonWidth 100

#define kDefaultLabelMargin 30.0
#define kDefaultTopMargin 32.0
#define kContainerHeight 50.0
#define kDefaultMargin 8.0

#define kBackButtonWidth 32.0

#define kGenericElementWidth 128.0
#define kGenericElementHeight 32.0
#define kGenericElementDefaultColor [UIColor lightGrayColor];

#define kBlackColor [UIColor blackColor]

#define kFontSize 17
#define kBorderWidth 1.0
#define kCornerRadius 5.0
#define kBorderColor [UIColor darkGrayColor].CGColor

#define kAnimationTime 0.5

#define kDidChangeLoggedUserNotification @"didChangeLoggedUserNotification"
#define kDidReceiveLocalNotification @"didReceiveLocalNotification"

#define HEIGHT(v)                                       v.frame.size.height
#define WIDTH(v)                                        v.frame.size.width
#define BOTTOM(v)                                       (v.frame.origin.y + v.frame.size.height)
#define AFTER(v)                                        (v.frame.origin.x + v.frame.size.width)
#define PH                                              [[UIScreen mainScreen].bounds.size.height]
#define PW                                              [[UIScreen mainScreen].bounds.size.width]

#define INNER_PADDING 10
#define SIDE_PADDING 15
#define LEGEND_VIEW 15
#define OFFSET_X 30
#define OFFSET_Y 30
#define OFFSET_PADDING 5

#define DEG2RAD(angle) angle*M_PI/180.0

@interface Utils : NSObject

@end
