//
//  SettingsManager.m
//  GuessMe
//
//  Created by Daniel Ilisei on 12/2/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "SettingsManager.h"

@implementation SettingsManager

static SettingsManager *settingsManager;

#pragma mark - Class methods

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        settingsManager = [[SettingsManager alloc] init];
    });
    
    return settingsManager;
}

#pragma mark - Lifecycle methods

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.settingOptions = [[NSMutableArray alloc] init];
        [self populateWithSettings];
    }
    
    return self;
}

#pragma mark - Private methods

- (void)populateWithSettings {
    [self.settingOptions addObject:@"Users"];
    [self.settingOptions addObject:@"Statistics"];
}

@end
