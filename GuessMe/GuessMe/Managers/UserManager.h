//
//  UserManager.h
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "User.h"

#define UserManagerInstance [UserManager sharedInstance]

typedef NS_ENUM(NSInteger, UserLoginResult) {
    UserLoginResultSuccessful,
    UserLoginResultUsernameNotFound,
    UserLoginResultWrongPassword
};

@interface UserManager : NSObject

@property (nonatomic, strong) NSMutableArray        *users;
@property (nonatomic, strong) User                  *currentLoggedUser;
@property (nonatomic, assign) UserLoginResult       loginResult;

/** Singleton instance of the class.
 */
+ (instancetype)sharedInstance;

/** Tries to login with the given credentials. Result will be stored in self.loginResult.
 */
- (void)loginWithUsername:(NSString *)username andPassword:(NSString *)password;

/** Determines if a given username is already used.
    @param username The username that needs to be checked wheter it exists or not.
 */
- (BOOL)usernameExists:(NSString *)username;

/** Adds an user.
 @param username The new user's username.
 */
- (BOOL)addUser:(User *)user;

/** Removes an user.
 @param user User to be removed.
 */
- (void)removeUser:(User *)user;

/** Updates an user.
 @param oldUser The user to be modified as it was before any modification.
 @param newUser The user to be modified as it should be after modifications.
 */
- (void)updateUser:(User *)oldUser withUser:(User *)newUser;
@end
