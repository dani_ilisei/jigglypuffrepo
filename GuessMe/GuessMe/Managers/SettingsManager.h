//
//  SettingsManager.h
//  GuessMe
//
//  Created by Daniel Ilisei on 12/2/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SettingsManagerInstance [SettingsManager sharedInstance]

@interface SettingsManager : NSObject

@property (nonatomic, strong) NSMutableArray        *settingOptions;

/** Singleton instance of the class.
 */
+ (instancetype)sharedInstance;

@end
