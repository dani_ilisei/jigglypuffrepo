//
//  UserManager.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "UserManager.h"
#import "Utils.h"
#import "AppDelegate.h"
#import "CoreData/CoreData.h"

#define kUsername @"username"
#define kPassword @"password"
#define kMail @"mail"
#define kPlayer @"player"

@interface UserManager()

@end

@implementation UserManager

static UserManager *userManager;

#pragma mark - Class methods

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        userManager = [[UserManager alloc] init];
    });
    return userManager;
}

#pragma mark - Lifecycle methods

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.users = [[NSMutableArray alloc] init];
        [self populateWithUsers];
    }
    
    return self;
}

#pragma mark - Setter methods
    
- (void)setCurrentLoggedUser:(User *)currentLoggedUser {
    _currentLoggedUser = currentLoggedUser;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kDidChangeLoggedUserNotification object:nil];
}
    
#pragma mark - Public methods

- (void)loginWithUsername:(NSString *)username andPassword:(NSString *)password {
    username = [username lowercaseString];
    password = [password lowercaseString];
    
    for (User *user in self.users) {
        
        if (![[user.userName lowercaseString] isEqualToString:username]) {
            if (!self.loginResult) {
                self.loginResult = UserLoginResultUsernameNotFound;
            }
            
        } else if ([[user.userName lowercaseString] isEqualToString:username] && ![[user.password lowercaseString] isEqualToString:password]) {
            self.loginResult = UserLoginResultWrongPassword;
            
        } else if ([[user.userName lowercaseString] isEqualToString:username] && [[user.password lowercaseString] isEqualToString:password]) {
            self.loginResult = UserLoginResultSuccessful;
            self.currentLoggedUser = user;
            break;
        }
    }
}

- (BOOL)usernameExists:(NSString *)username {
    for (User *user in self.users) {
        if ([user.userName isEqualToString:username]) {
            return YES;
        }
    }
    
    return NO;
}

- (BOOL)addUser:(User *)user {
    for (User *savedUser in self.users) {
        if ([user.userName isEqualToString:savedUser.userName]) {
            return NO;
        }
    }
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSManagedObject *userToBeSaved = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:managedObjectContext];
    
    [userToBeSaved setValue:user.userName forKey:kUsername];
    [userToBeSaved setValue:user.password forKey:kPassword];
    [userToBeSaved setValue:user.mail forKey:kMail];
    if (user.player) {
        [userToBeSaved setValue:@"YES" forKey:kPlayer];
    } else {
        [userToBeSaved setValue:@"NO" forKey:kPlayer];
    }
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't save after add! %@ %@", error, [error localizedDescription]);
        
    } else {
        [self.users addObject:user];
    }
    return YES;
}

- (void)removeUser:(User *)user {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *savedUsers = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for (int i = 0; i < savedUsers.count; i++) {
        NSManagedObject *managedUser = [savedUsers objectAtIndex:i];
        if ([user.userName isEqualToString:[managedUser valueForKey:kUsername]]) {
            [managedObjectContext deleteObject:managedUser];
            [self.users removeObject:user];
            
            NSError *error = nil;
            if (![managedObjectContext save:&error]) {
                NSLog(@"Can't save after delete! %@ %@", error, [error localizedDescription]);
            }
            
            break;
        }
    }
}

- (void)updateUser:(User *)oldUser withUser:(User *)newUser {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *savedUsers = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for (int i = 0; i < savedUsers.count; i++) {
        NSManagedObject *managedUser = [savedUsers objectAtIndex:i];
        
        if ([oldUser.userName isEqualToString:[managedUser valueForKey:kUsername]]) {
            [managedUser setValue:newUser.userName forKey:kUsername];
            [managedUser setValue:newUser.password forKey:kPassword];
            [managedUser setValue:newUser.mail forKey:kMail];
            if (newUser.player) {
                [managedUser setValue:@"YES" forKey:kPlayer];
            } else {
                [managedUser setValue:@"NO" forKey:kPlayer];
            }
            
            NSError *error = nil;
            if (![managedObjectContext save:&error]) {
                NSLog(@"Can't save after add! %@ %@", error, [error localizedDescription]);
                
            } else {
                User *user = [self.users objectAtIndex:i];
                user.userName = newUser.userName;
                user.password = newUser.password;
                user.mail = newUser.mail;
                user.player = newUser.player;
            }
            
            break;
        }
    }
}
#pragma mark - Private methods

- (void)populateWithUsers {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"User"];
    NSArray *savedUsers = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for (int i = 0; i < savedUsers.count; i++) {
        NSManagedObject *managedUser = [savedUsers objectAtIndex:i];
        
        User *user = [[User alloc] init];
        user.userName = [managedUser valueForKey:kUsername];
        user.password = [managedUser valueForKey:kPassword];
        user.mail = [managedUser valueForKey:kMail];
        if ([[managedUser valueForKey:kPlayer] isEqualToString:@"YES"]) {
            user.player = YES;
        } else {
            user.player = NO;
        }
        
        [self.users addObject:user];
    }
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
