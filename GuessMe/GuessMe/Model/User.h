//
//  User.h
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString          *userName;
@property (nonatomic, strong) NSString          *password;
@property (nonatomic, strong) NSString          *mail;
@property (nonatomic, assign) BOOL              player;

@end
