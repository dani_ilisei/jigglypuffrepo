//
//  User.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "UserManager.h"

#import "User.h"

@implementation User

#pragma mark - Lifecycle methods

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.userName = @"";
        self.password = @"";
        self.mail = @"";
        self.player = YES;
    }
    
    return self;
}


@end
