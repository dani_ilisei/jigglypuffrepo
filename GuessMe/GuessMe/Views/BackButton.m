//
//  BackButton.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/12/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "BackButton.h"

#import "Utils.h"

@implementation BackButton

#pragma mark - Lifecycle methods

- (instancetype)initWithFrame:(CGRect)frame {
   self = [super initWithFrame:frame];
    
    if (self) {
        [self setupUI];
    }
    
    return self;
}

#pragma mark - UI & Action methods

- (void)setupUI {
    [self setBackgroundImage:[UIImage imageNamed:@"back_icon"] forState:UIControlStateNormal];
    self.tintColor = [UIColor blackColor];
    self.layer.borderWidth = kBorderWidth;
    self.layer.cornerRadius = kCornerRadius;
    self.layer.borderColor = kBorderColor;
}

@end
