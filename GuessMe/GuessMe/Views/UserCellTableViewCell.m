//
//  UserCellTableViewCell.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "UserCellTableViewCell.h"
#import "UserManager.h"
#import "Utils.h"

#define kDefaultWidthMargin 8

@interface UserCellTableViewCell()

@property (nonatomic, strong) UILabel               *userNameLabel;

@property (nonatomic, strong) UIButton              *removeButton;

@end

@implementation UserCellTableViewCell

#pragma mark - Lifecycle methods

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        [self setupUI];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Setters

- (void)setUser:(User *)user {
    _user = user;
    
    [self.userNameLabel setText:user.userName];
}

#pragma mark - Public methods

- (void)hideCell {
    self.frame = CGRectMake(0.0, 0.0, self.frame.size.width, 0.0);
}

- (void)showRemoveButton {
    [UIView animateWithDuration:kAnimationTime animations:^{
        self.removeButton.frame = CGRectMake(self.frame.size.width - self.removeButton.frame.size.width, self.removeButton.frame.origin.y, self.removeButton.frame.size.width, self.removeButton.frame.size.height);
    }];
}

- (void)hideRemoveButton {
    [UIView animateWithDuration:kAnimationTime animations:^{
        self.removeButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width, self.removeButton.frame.origin.y, self.removeButton.frame.size.width, self.removeButton.frame.size.height);
    }];
}

#pragma mark - UI & action methods

- (void)setupUI {
    [self setUserNameLabel];
    [self setupRemoveButton];
}

- (void)setUserNameLabel {
    self.userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultWidthMargin, 3.0, self.frame.size.width - 2 * kDefaultWidthMargin, self.frame.size.height - 6.0)];
    
    [self.userNameLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
    [self.userNameLabel setText:@""];
    [self.userNameLabel setTextColor:[UIColor blackColor]];
    self.userNameLabel.textAlignment = NSTextAlignmentLeft;
    
    [self addSubview:self.userNameLabel];
}

- (void)setupRemoveButton {
    self.removeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width, 0.0, 100.0, self.frame.size.height)];
    
    [self.removeButton setTitle:@"Remove" forState:UIControlStateNormal];
    [self.removeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.removeButton setBackgroundColor:[UIColor blackColor]];
    
    [self.removeButton addTarget:self action:@selector(removeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.removeButton];
}

- (void)removeButtonPressed:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(userTableViewCellDidSelectRemoveButton:withUser:)]) {
        [self.delegate userTableViewCellDidSelectRemoveButton:self withUser:self.user];
    }
}

@end
