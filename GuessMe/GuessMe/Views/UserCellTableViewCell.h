//
//  UserCellTableViewCell.h
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@protocol UserTableViewCellDelegate;

@interface UserCellTableViewCell : UITableViewCell

@property (nonatomic, strong) User                  *user;

@property (nonatomic, weak) id<UserTableViewCellDelegate> delegate;

- (void)hideCell;
- (void)showRemoveButton;
- (void)hideRemoveButton;
@end

@protocol UserTableViewCellDelegate <NSObject>
@optional
- (void)userTableViewCellDidSelectRemoveButton:(UserCellTableViewCell *)cell withUser:(User *)user;

@end
