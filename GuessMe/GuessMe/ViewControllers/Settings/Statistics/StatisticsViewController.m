//
//  StatisticaViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 12/20/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "StatisticsViewController.h"
#import "UserManager.h"
#import "CircularChart.h"
#import "LegendView.h"
#import "BackButton.h"

#import "Utils.h"

#import "User.h"

#define kTitleWidth 200

@interface StatisticsViewController () <CircularChartDataSource, CircularChartDelegate>

@property (nonatomic, strong) BackButton        *backButton;
@property (nonatomic, strong) UILabel           *titleLabel;

@end

@implementation StatisticsViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CircularChartDataSource methods
- (CGFloat)strokeWidthForCircularChart{
    return 50;
}

- (NSInteger)numberOfValuesForCircularChart{
    return 2;
}

- (UIColor *)colorForValueInCircularChartWithIndex:(NSInteger)lineNumber{
    NSInteger aRedValue = arc4random()%255;
    NSInteger aGreenValue = arc4random()%255;
    NSInteger aBlueValue = arc4random()%255;
    UIColor *randColor = [UIColor colorWithRed:aRedValue/255.0f green:aGreenValue/255.0f blue:aBlueValue/255.0f alpha:1.0f];
    return randColor;
}

- (NSString *)titleForValueInCircularChartWithIndex:(NSInteger)index{
    if (index == 0) {
        return @"Players";
    
    } else {
        return @"Challengers";
    }
}

- (NSNumber *)valueInCircularChartWithIndex:(NSInteger)index{
    int players = 0;
    
    for (User *user in UserManagerInstance.users) {
        if (user.player) {
            players += 1;
        }
    }
    
    if (index == 0) {
        
        return [NSNumber numberWithInteger:players];
    
    } else {
        
        return [NSNumber numberWithInteger:UserManagerInstance.users.count - players];
    }
}

- (UIView *)customViewForCircularChartTouchWithValue:(NSNumber *)value{
    int players = 0;
    
    for (User *user in UserManagerInstance.users) {
        if (user.player) {
            players += 1;
        }
    }
    
    NSString *result = @"Challengers: ";
    if (value.integerValue == players) {
        result = @"Players: ";
    }
    
    
    UIView *view = [[UIView alloc] init];
    [view setBackgroundColor:[UIColor whiteColor]];
    [view.layer setCornerRadius:4.0F];
    [view.layer setBorderWidth:1.0F];
    [view.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [view.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [view.layer setShadowRadius:2.0F];
    [view.layer setShadowOpacity:0.3F];
    
    UILabel *label = [[UILabel alloc] init];
    [label setFont:[UIFont systemFontOfSize:12]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setText:[NSString stringWithFormat:@"%@%@", result, value]];
    [label setFrame:CGRectMake(0, 0, 100, 30)];
    [label setAdjustsFontSizeToFitWidth:TRUE];
    [view addSubview:label];
    
    [view setFrame:label.frame];
    return view;
}

#pragma mark - CircularChartDelegate methods

- (void)didTapOnCircularChartWithValue:(NSString *)value{
    NSLog(@"Circular Chart: %@",value);
}

#pragma mark - UI & action methods

- (void)setupUI {
    [self setupView];
    
    [self setupBackButton];
    [self setupTitleLabel];
    
    [self createCircularChart];
}

- (void)setupView {
    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)setupBackButton {
    self.backButton = [[BackButton alloc] initWithFrame:CGRectMake(kBackButtonWidth / 2, kBackButtonWidth, kBackButtonWidth, kBackButtonWidth)];
    
    [self.backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
}
    
- (void)setupTitleLabel {
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width - kTitleWidth) / 2, kBackButtonWidth, kTitleWidth, kBackButtonWidth)];
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:24];
    self.titleLabel.text = @"Statistics";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:self.titleLabel];
}

- (void)createCircularChart{
    CircularChart *chart = [[CircularChart alloc] initWithFrame:CGRectMake(0.0, CGRectGetMaxY(self.backButton.frame) + 20.0, self.view.frame.size.width, self.view.frame.size.height - (CGRectGetMaxY(self.backButton.frame) + 20.0))];
    [chart setDataSource:self];
    [chart setDelegate:self];
    
    [chart setShowLegend:TRUE];
    [chart setLegendViewType:LegendTypeHorizontal];
    
    [chart setTextFontSize:12];
    [chart setTextColor:[UIColor blackColor]];
    [chart setTextFont:[UIFont systemFontOfSize:chart.textFontSize]];
    
    [chart setShowCustomMarkerView:TRUE];
    
    [chart drawCircularChart];
    [self.view addSubview:chart];
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
