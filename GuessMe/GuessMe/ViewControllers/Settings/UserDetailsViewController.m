//
//  UserDetailsViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/11/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "UserDetailsViewController.h"
#import "UserChangePasswordViewController.h"

#import "MessageUI/MessageUI.h"

#import "BackButton.h"

#import "UserManager.h"
#import "Utils.h"

@interface UserDetailsViewController () <MFMailComposeViewControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) BackButton            *backButton;

@property (nonatomic, strong) UILabel               *usernameDescriptiveLabel;
@property (nonatomic, strong) UITextField           *usernameTextField;

@property (nonatomic, strong) UILabel               *playerRoleDescriptiveLabel;
@property (nonatomic, strong) UILabel               *playerRoleLabel;

@property (nonatomic, strong) UIButton              *changePasswordButton;
@property (nonatomic, strong) UIButton              *sendMailButton;

@end

@implementation UserDetailsViewController

#pragma mark - Lifecycle methods

- (instancetype)initWithUser:(User *)user {
    self = [super init];
    
    if (self) {
        self.user = user;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - MFMailComposeViewControllerDelegate methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self changeSendMailButtonFunction:NO];
    
    return YES;
}

#pragma mark - UI & action methods

- (void)setupUI {
    [self setupView];
    
    [self setupBackButton];
    
    [self setupUsernameDescritiveLabel];
    [self setupUsernameTextField];
    
    [self setupPlayerRoleDescriptiveLabel];
    [self setupPlayerRoleLabel];
    
    [self setupChangePasswordButton];
    [self setupSendMailButton];
}

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)setupBackButton {
    self.backButton = [[BackButton alloc] initWithFrame:CGRectMake(kBackButtonWidth / 2, kBackButtonWidth, kBackButtonWidth, kBackButtonWidth)];
    
    [self.backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
}

- (void)setupUsernameDescritiveLabel {
    self.usernameDescriptiveLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultLabelMargin, self.view.frame.size.height / 4, kLabelWidth, kContainerHeight)];
    [self.usernameDescriptiveLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.usernameDescriptiveLabel setTextColor:[UIColor blackColor]];
    [self.usernameDescriptiveLabel setText:@"Username: "];
    
    [self.view addSubview:self.usernameDescriptiveLabel];
}

- (void)setupUsernameTextField {
    self.usernameTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.usernameDescriptiveLabel.frame) + kDefaultMargin, self.usernameDescriptiveLabel.frame.origin.y, self.view.frame.size.width - 2 * kDefaultLabelMargin - kDefaultMargin - self.usernameDescriptiveLabel.frame.size.width, self.usernameDescriptiveLabel.frame.size.height)];
    
    [self.usernameTextField setText:self.user.userName];
    
    self.usernameTextField.textAlignment = NSTextAlignmentCenter;
    self.usernameTextField.layer.cornerRadius = kCornerRadius;
    self.usernameTextField.layer.borderWidth = kBorderWidth;
    self.usernameTextField.layer.borderColor = kBorderColor;
    
    self.usernameTextField.delegate = self;
    
    [self.view addSubview:self.usernameTextField];
}

- (void)setupPlayerRoleDescriptiveLabel {
    self.playerRoleDescriptiveLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultLabelMargin, CGRectGetMaxY(self.usernameDescriptiveLabel.frame) + 20.0, kLabelWidth, kContainerHeight)];
    [self.playerRoleDescriptiveLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.playerRoleDescriptiveLabel setTextColor:[UIColor blackColor]];
    [self.playerRoleDescriptiveLabel setText:@"Role: "];
    
    [self.view addSubview:self.playerRoleDescriptiveLabel];
}

- (void)setupPlayerRoleLabel {
    self.playerRoleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.playerRoleDescriptiveLabel.frame) + kDefaultMargin, self.playerRoleDescriptiveLabel.frame.origin.y, self.view.frame.size.width - 2 * kDefaultLabelMargin - kDefaultMargin - self.playerRoleDescriptiveLabel.frame.size.width, self.playerRoleDescriptiveLabel.frame.size.height)];
    
    if (self.user.player) {
        [self.playerRoleLabel setText:@"Player"];
        
    } else {
        [self.playerRoleLabel setText:@"Challenger"];
    }
    
    self.playerRoleLabel.textAlignment = NSTextAlignmentCenter;
    self.playerRoleLabel.layer.cornerRadius = kCornerRadius;
    self.playerRoleLabel.layer.borderWidth = kBorderWidth;
    self.playerRoleLabel.layer.borderColor = kBorderColor;
    
    [self.view addSubview:self.playerRoleLabel];
}

- (void)setupChangePasswordButton {
    self.changePasswordButton = [[UIButton alloc] initWithFrame:CGRectMake(self.playerRoleLabel.frame.origin.x, CGRectGetMaxY(self.playerRoleLabel.frame) + 20.0, self.playerRoleLabel.frame.size.width, self.playerRoleLabel.frame.size.height)];
    [self.changePasswordButton setBackgroundColor:[UIColor blackColor]];
    [self.changePasswordButton setTitle:@"Change password" forState:UIControlStateNormal];
    [self.changePasswordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.changePasswordButton.layer.cornerRadius = kCornerRadius;
    self.changePasswordButton.layer.borderWidth = kBorderWidth;;
    self.changePasswordButton.layer.borderColor = kBorderColor;
    
    [self.changePasswordButton addTarget:self action:@selector(changePasswordButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.changePasswordButton];
}

- (void)setupSendMailButton {
    self.sendMailButton = [[UIButton alloc] initWithFrame:CGRectMake(self.changePasswordButton.frame.origin.x, CGRectGetMaxY(self.changePasswordButton.frame) + 20.0, self.changePasswordButton.frame.size.width, self.changePasswordButton.frame.size.height)];
    self.sendMailButton.backgroundColor = [UIColor blackColor];
    [self.sendMailButton setTitle:@"Send mail" forState:UIControlStateNormal];
    [self.sendMailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.sendMailButton.layer.cornerRadius = kCornerRadius;
    self.sendMailButton.layer.borderWidth = kBorderWidth;
    self.sendMailButton.layer.borderColor = kBorderColor;
    
    [self.sendMailButton addTarget:self action:@selector(sendMailButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.sendMailButton];
}

- (void)changeSendMailButtonFunction:(BOOL)sendMail {
    if (sendMail) {
        self.sendMailButton.tag = 0;
        [self.sendMailButton setTitle:@"Send mail" forState:UIControlStateNormal];
        
        [UIView animateWithDuration:0.5 animations:^{
            [self.sendMailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.sendMailButton setBackgroundColor:kBlackColor];
        }];
        
    } else {
        self.sendMailButton.tag = 1;
        [self.sendMailButton setTitle:@"Done" forState:UIControlStateNormal];
        
        [UIView animateWithDuration:0.5 animations:^{
            [self.sendMailButton setTitleColor:kBlackColor forState:UIControlStateNormal];
            [self.sendMailButton setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.5]];
        }];
    }
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changePasswordButtonPressed:(UIButton *)sender {
    UserChangePasswordViewController *userChangePasswordViewController = [[UserChangePasswordViewController alloc] init];
    userChangePasswordViewController.user = self.user;
    
    [self.navigationController pushViewController:userChangePasswordViewController animated:YES];
}

- (void)sendMailButtonPressed:(UIButton *)sender {
    
    if (sender.tag) {
        
        if (![self.usernameTextField.text isEqualToString:@""]) {
            [self changeSendMailButtonFunction:YES];
            
            User* user = [[User alloc] init];
            user.userName = self.usernameTextField.text;
            user.password = self.user.password;
            user.mail = self.user.mail;
            user.player = self.user.player;
            [UserManagerInstance updateUser:self.user withUser:user];
            
            [self.usernameTextField resignFirstResponder];
            
        } else {
            UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Error" message:@"Username cannot be emtpy!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:alertVC completion:nil];
            }];
            
            [alertVC addAction:okAction];
            
            [self presentViewController:alertVC animated:YES completion:nil];
        }
    
    } else {
        NSString *mailTitle = @"Title";
        NSString *mailBody = @"Body";
        NSArray *recipents = [NSArray arrayWithObject:self.user.mail];
        
        MFMailComposeViewController *mailVC = [MFMailComposeViewController new];
        mailVC.mailComposeDelegate = self;
        [mailVC setSubject:mailTitle];
        [mailVC setMessageBody:mailBody isHTML:NO];
        [mailVC setToRecipients:recipents];
        
        [self presentViewController:mailVC animated:YES completion:nil];
    }
}

@end
