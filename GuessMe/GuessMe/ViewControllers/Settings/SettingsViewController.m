//
//  SettingsViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 12/2/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "SettingsViewController.h"
#import "UsersTableViewController.h"
#import "StatisticsViewController.h"

#import "BackButton.h"

#import "SettingsManager.h"
#import "UserManager.h"
#import "Utils.h"

@interface SettingsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) BackButton            *backButton;
@property (nonatomic, strong) UITableView           *tableView;

@end

@implementation SettingsViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return SettingsManagerInstance.settingOptions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settingsCellIdentifier"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [SettingsManagerInstance.settingOptions objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        UsersTableViewController *usersTableViewController = [[UsersTableViewController alloc] init];
        
        [self.navigationController pushViewController:usersTableViewController animated:YES];
    
    } else if (indexPath.row == 1) {
        StatisticsViewController *statisticsViewController = [[StatisticsViewController alloc] init];
        
        [self.navigationController pushViewController:statisticsViewController animated:YES];
    }
}

#pragma mark - UI & action methods

- (void)setupUI {
    [self setupView];
    
    [self setupBackButton];
    
    [self setupTableView];
}

- (void)setupView {
    self.view.backgroundColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setupBackButton {
    self.backButton = [[BackButton alloc] initWithFrame:CGRectMake(kBackButtonWidth / 2, kBackButtonWidth, kBackButtonWidth, kBackButtonWidth)];
    
    [self.backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
}

- (void)setupTableView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, CGRectGetMaxY(self.backButton.frame) + 20.0, self.view.frame.size.width, self.view.frame.size.height - CGRectGetMaxY(self.backButton.frame))];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.view addSubview:self.tableView];
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
