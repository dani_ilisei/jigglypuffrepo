//
//  UsersTableViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/21/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "UsersTableViewController.h"
#import "UserDetailsViewController.h"
#import "AddUserViewController.h"

#import "UserCellTableViewCell.h"
#import "BackButton.h"
#import "Utils.h"

#import "UserManager.h"
#import "User.h"

#define kBackgroundColor [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]

@interface UsersTableViewController () <UITableViewDelegate, UITableViewDataSource, UserTableViewCellDelegate, AddUserViewControllerDelegate>

@property (nonatomic, strong) BackButton            *backButton;
@property (nonatomic, strong) UIButton              *addButton;
@property (nonatomic, strong) UIButton              *deleteButton;

@property (nonatomic, strong) UITableView           *tableView;

@property (nonatomic, assign) BOOL                  continueBool;

@end

@implementation UsersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.deleteButton.tag = 0;
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return UserManagerInstance.users.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userCellIdentifier"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    User *user = [UserManagerInstance.users objectAtIndex:indexPath.row];
    cell.user = user;
    cell.delegate = self;
    
    if (self.deleteButton.tag) {
        [cell showRemoveButton];
    
    } else {
        [cell hideRemoveButton];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    User *user = [UserManagerInstance.users objectAtIndex:indexPath.row];
    
    UserDetailsViewController *userDetailsViewController = [[UserDetailsViewController alloc] initWithUser:user];
    
    [self.navigationController pushViewController:userDetailsViewController animated:YES];
}

#pragma mark - UserTableViewCellDelegate methods

- (void)userTableViewCellDidSelectRemoveButton:(UserCellTableViewCell *)cell withUser:(User *)user {
    [UserManagerInstance removeUser:user];
    
    [UIView animateWithDuration:kAnimationTime animations:^{
        [cell hideCell];
    } completion:^(BOOL finished) {
        [self.tableView reloadData];
    }];
}

#pragma mark - AddUserViewControllerDelegate methods

- (void)addUserViewControllerDidTryToAddUserWithResult:(AddUserResult)result {
    if (result == AddUserResultSuccessful) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Success" message:@"User was added successfully!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:alertVC completion:nil];
        }];
        
        [alertVC addAction:okAction];
        
        [self presentViewController:alertVC animated:YES completion:nil];

    }
}

#pragma mark - UI & action methods

- (void)setupUI {
    [self setupView];
    
    [self setupBackButton];
    [self setupAddButton];
    [self setupDeleteButton];
    
    [self setupTableView];
}

- (void)setupView {
    self.view.backgroundColor = kBackgroundColor;
    self.navigationController.navigationBarHidden = YES;
}

- (void)setupBackButton {
    self.backButton = [[BackButton alloc] initWithFrame:CGRectMake(kBackButtonWidth / 2, kBackButtonWidth, kBackButtonWidth, kBackButtonWidth)];
    
    [self.backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
}

- (void)setupAddButton {
    self.addButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 3 * kBackButtonWidth / 2 , kBackButtonWidth, kBackButtonWidth, kBackButtonWidth)];
    
    self.addButton.layer.borderWidth = kBorderWidth;
    self.addButton.layer.cornerRadius = kCornerRadius;
    self.addButton.layer.borderColor = kBorderColor;
    self.addButton.backgroundColor = [UIColor clearColor];
    [self.addButton setTitle:@"+" forState:UIControlStateNormal];
    [self.addButton setTitleColor:kBlackColor forState:UIControlStateNormal];
    
    [self.addButton addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.addButton];
}

- (void)setupDeleteButton {
    self.deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(self.addButton.frame.origin.x - kDefaultMargin - kGenericElementWidth, kBackButtonWidth, kGenericElementWidth, kBackButtonWidth)];
    
    self.deleteButton.layer.borderWidth = kBorderWidth;
    self.deleteButton.layer.cornerRadius = kCornerRadius;
    self.deleteButton.layer.borderColor = kBorderColor;
    self.deleteButton.backgroundColor = [UIColor clearColor];
    [self.deleteButton setTitle:@"Remove" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:kBlackColor forState:UIControlStateNormal];
    
    [self.deleteButton addTarget:self action:@selector(editButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.deleteButton];
}

- (void)setupTableView {
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, CGRectGetMaxY(self.backButton.frame) + 20.0, self.view.frame.size.width, self.view.frame.size.height - 50.0)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerClass:[UserCellTableViewCell class] forCellReuseIdentifier:@"userCellIdentifier"];
    
    [self.view addSubview:self.tableView];
}

- (void)configureEditButton {
    if (self.deleteButton.tag) {
        [UIView animateWithDuration:kAnimationTime animations:^{
            self.deleteButton.backgroundColor = kBlackColor;
            [self.deleteButton setTitleColor:kBackgroundColor forState:UIControlStateNormal];
        }];
    
    } else {
        [UIView animateWithDuration:kAnimationTime animations:^{
            self.deleteButton.backgroundColor = [UIColor clearColor];
            [self.deleteButton setTitleColor:kBlackColor forState:UIControlStateNormal];
        }];
    }
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addButtonPressed:(UIButton *)sender {
    AddUserViewController *addUserViewController = [[AddUserViewController alloc] init];
    addUserViewController.delegate = self;
    [self.navigationController pushViewController:addUserViewController animated:YES];
}

- (void)editButtonPressed:(UIButton *)sender {
    
    if (self.deleteButton.tag) {
        self.deleteButton.tag = 0;
    
    } else {
        self.deleteButton.tag = 1;
    }
    
    [self configureEditButton];
    [self.tableView reloadData];
}

@end
