//
//  UserChangePasswordViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/12/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "UserChangePasswordViewController.h"

#import "UserManager.h"

#import "BackButton.h"

#import "Utils.h"

@interface UserChangePasswordViewController ()

@property (nonatomic, strong) BackButton        *backButton;

@property (nonatomic, strong) UILabel           *oldPasswordLabel;
@property (nonatomic, strong) UITextField       *oldPasswordTextField;

@property (nonatomic, strong) UILabel           *passwordLabel;
@property (nonatomic, strong) UITextField       *passwordTexField;

@property (nonatomic, strong) UIButton          *submitButton;

@property (nonatomic, assign) BOOL              passwordChangedSuccessfully;

@end

@implementation UserChangePasswordViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _passwordChangedSuccessfully = NO;
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Private methods

- (void)showAlertWithTitle:(NSString *)title andBody:(NSString *)body {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:body preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:alertVC completion:nil];
        
        if (self.passwordChangedSuccessfully) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    [alertVC addAction:okAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - UI & action methods

- (void)setupUI {
    [self setupView];
    
    [self setupBackButton];
    
    [self setupOldPasswordLabel];
    [self setupOldPasswordTextField];
    
    [self setupNewPasswordLabel];
    [self setupNewPasswordTextField];
    
    [self setupSubmitButton];
}

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)setupBackButton {
    self.backButton = [[BackButton alloc] initWithFrame:CGRectMake(kBackButtonWidth / 2, kBackButtonWidth, kBackButtonWidth, kBackButtonWidth)];
    
    [self.backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
}

- (void)setupOldPasswordLabel {
    self.oldPasswordLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultLabelMargin, self.view.frame.size.height / 4, kLabelWidth, kContainerHeight)];
    [self.oldPasswordLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:17]];
    [self.oldPasswordLabel setTextColor:[UIColor blackColor]];
    [self.oldPasswordLabel setText:@"Old password:"];
    self.oldPasswordLabel.textAlignment = NSTextAlignmentLeft;
    self.oldPasswordLabel.adjustsFontSizeToFitWidth = YES;
    
    [self.view addSubview:self.oldPasswordLabel];
}

- (void)setupOldPasswordTextField {
    self.oldPasswordTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.oldPasswordLabel.frame) + kDefaultMargin, self.oldPasswordLabel.frame.origin.y, self.view.frame.size.width - 2 * kDefaultLabelMargin - kDefaultMargin - self.oldPasswordLabel.frame.size.width, self.oldPasswordLabel.frame.size.height)];
    self.oldPasswordTextField.placeholder = @"  Enter old password ..";
    self.oldPasswordTextField.layer.cornerRadius = kCornerRadius;
    self.oldPasswordTextField.layer.borderWidth = kBorderWidth;
    self.oldPasswordTextField.layer.borderColor = kBorderColor;
    self.oldPasswordTextField.secureTextEntry = YES;
    
    [self.view addSubview:self.oldPasswordTextField];
}

- (void)setupNewPasswordLabel {
    self.passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultLabelMargin, CGRectGetMaxY(self.oldPasswordTextField.frame) + 20.0, self.oldPasswordLabel.frame.size.width, self.oldPasswordLabel.frame.size.height)];
    [self.passwordLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:17]];
    [self.passwordLabel setTextColor:[UIColor blackColor]];
    [self.passwordLabel setText:@"New password:"];
    self.passwordLabel.textAlignment = NSTextAlignmentLeft;
    self.passwordLabel.adjustsFontSizeToFitWidth = YES;
    
    [self.view addSubview:self.passwordLabel];
}

- (void)setupNewPasswordTextField {
    self.passwordTexField = [[UITextField alloc] initWithFrame:CGRectMake(self.oldPasswordTextField.frame.origin.x, CGRectGetMaxY(self.oldPasswordTextField.frame) + 20.0, self.oldPasswordTextField.frame.size.width, self.oldPasswordTextField.frame.size.height)];
    self.passwordTexField.placeholder = @"  Enter new password ..";
    self.passwordTexField.layer.cornerRadius = kCornerRadius;
    self.passwordTexField.layer.borderWidth = kBorderWidth;
    self.passwordTexField.layer.borderColor = kBorderColor;
    self.passwordTexField.secureTextEntry = YES;
    
    [self.view addSubview:self.passwordTexField];
}

- (void)setupSubmitButton {
    self.submitButton = [[UIButton alloc] initWithFrame:CGRectMake(self.passwordTexField.frame.origin.x, CGRectGetMaxY(self.passwordTexField.frame) + 20.0, self.passwordTexField.frame.size.width, self.passwordTexField.frame.size.height)];
    [self.submitButton setBackgroundColor:[UIColor blackColor]];
    [self.submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    [self.submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.submitButton.layer.cornerRadius = kCornerRadius;
    self.submitButton.layer.borderWidth = kBorderWidth;
    self.submitButton.layer.borderColor = kBorderColor;
    
    [self.submitButton addTarget:self action:@selector(submitButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.submitButton];
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)submitButtonPressed:(UIButton *)sender {
    
    if (![self.oldPasswordTextField.text isEqualToString:@""]) {
        
        if ([self.oldPasswordTextField.text isEqualToString:self.user.password]) {
            User *user = [[User alloc] init];
            user.userName = self.user.userName;
            user.password = self.passwordTexField.text;
            user.mail = self.user.mail;
            user.player = self.user.player;
            
            [UserManagerInstance updateUser:self.user withUser:user];
            
            self.passwordChangedSuccessfully = YES;
            [self showAlertWithTitle:@"Success!" andBody:@"Password was changed successfully!"];
            
        } else {
            [self showAlertWithTitle:@"Error!" andBody:@"Old password does not match!"];
        }
    
    } else {
        [self showAlertWithTitle:@"Error!" andBody:@"Old password cannot be empty!"];
    }
    
    self.oldPasswordTextField.text = @"";
    self.passwordTexField.text = @"";
}

@end
