//
//  AddUserViewController.h
//  GuessMe
//
//  Created by Daniel Ilisei on 12/4/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "ViewController.h"

@class AddUserViewController;

typedef NS_ENUM(NSInteger, AddUserResult) {
    AddUserResultSuccessful,
    AddUserResultFailed
};

@protocol AddUserViewControllerDelegate;

@interface AddUserViewController : UIViewController

@property (nonatomic, weak) id<AddUserViewControllerDelegate> delegate;

@end

@protocol AddUserViewControllerDelegate <NSObject>
@optional
- (void)addUserViewControllerDidTryToAddUserWithResult:(AddUserResult)result;

@end
