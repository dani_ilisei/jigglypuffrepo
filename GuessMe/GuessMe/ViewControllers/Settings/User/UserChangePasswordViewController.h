//
//  UserChangePasswordViewController.h
//  GuessMe
//
//  Created by Daniel Ilisei on 11/12/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "User.h"

@interface UserChangePasswordViewController : UIViewController

@property (nonatomic, strong) User          *user;

@end
