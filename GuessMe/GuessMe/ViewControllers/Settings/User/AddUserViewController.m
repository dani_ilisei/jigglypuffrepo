//
//  AddUserViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 12/4/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "AddUserViewController.h"

#import "BackButton.h"
#import "Utils.h"

#import "User.h"

#import "UserManager.h"

@interface AddUserViewController ()

@property (nonatomic, strong) BackButton        *backButton;

@property (nonatomic, strong) UILabel           *usernameLabel;
@property (nonatomic, strong) UITextField       *usernameTextField;

@property (nonatomic, strong) UILabel           *passwordLabel;
@property (nonatomic, strong) UITextField       *passwordTextField;

@property (nonatomic, strong) UILabel           *mailLabel;
@property (nonatomic, strong) UITextField       *mailTextField;

@property (nonatomic, strong) UILabel           *playerLabel;
@property (nonatomic, strong) UISwitch          *playerSwitch;

@property (nonatomic, strong) UIButton          *submitButton;

@end

@implementation AddUserViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)configureView:(UIView *)view {
    view.layer.cornerRadius = kCornerRadius;
    view.layer.borderWidth = kBorderWidth;
    view.layer.borderColor = kBorderColor;
    view.backgroundColor = [UIColor whiteColor];
}

- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:alertVC completion:nil];
    }];
    
    [alertVC addAction:okAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - UI & action methods

- (void)setupUI {
    [self setupView];
    
    [self setupBackButton];
    
    [self setupUsernameLabel];
    [self setupUsernameTextField];
    
    [self setupPasswordLabel];
    [self setupPasswordTextField];
    
    [self setupMailLabel];
    [self setupMailTextField];
    
    [self setupPlayerLabel];
    [self setupPlayerSwitch];
    
    [self setupSubmitButton];
}

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setupBackButton {
    self.backButton = [[BackButton alloc] initWithFrame:CGRectMake(kBackButtonWidth / 2, kBackButtonWidth, kBackButtonWidth, kBackButtonWidth)];
    
    [self.backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
}

- (void)setupUsernameLabel {
    self.usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultLabelMargin, self.view.frame.size.height / 6, kLabelWidth, kContainerHeight)];
    
    [self.usernameLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.usernameLabel setTextColor:kBlackColor];
    [self.usernameLabel setText:@"Username: "];
    
    [self.view addSubview:self.usernameLabel];
}

- (void)setupUsernameTextField {
    self.usernameTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.usernameLabel.frame) + kDefaultMargin, self.usernameLabel.frame.origin.y, self.view.frame.size.width - 2 * kDefaultLabelMargin - kDefaultMargin - self.usernameLabel.frame.size.width, self.usernameLabel.frame.size.height)];
    
    [self configureView:self.usernameTextField];
    self.usernameTextField.textAlignment = NSTextAlignmentCenter;
    self.usernameTextField.placeholder = @" Username";
    
    [self.view addSubview:self.usernameTextField];
}

- (void)setupPasswordLabel {
    self.passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.usernameLabel.frame.origin.x, CGRectGetMaxY(self.usernameLabel.frame) + 20.0, self.usernameLabel.frame.size.width, self.usernameLabel.frame.size.height)];
    
    [self.passwordLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.passwordLabel setTextColor:kBlackColor];
    [self.passwordLabel setText:@"Password: "];
    
    [self.view addSubview:self.passwordLabel];
}

- (void)setupPasswordTextField {
    self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(self.usernameTextField.frame.origin.x, self.passwordLabel.frame.origin.y, self.usernameTextField.frame.size.width, self.usernameTextField.frame.size.height)];
    
    [self configureView:self.passwordTextField];
    self.passwordTextField.textAlignment = NSTextAlignmentCenter;
    self.passwordTextField.placeholder = @" Password";
    self.passwordTextField.secureTextEntry = YES;
    
    [self.view addSubview:self.passwordTextField];
}

- (void)setupMailLabel {
    self.mailLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.passwordLabel.frame.origin.x, CGRectGetMaxY(self.passwordLabel.frame) + 20.0, self.passwordLabel.frame.size.width, self.passwordLabel.frame.size.height)];
    
    [self.mailLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.mailLabel setTextColor:kBlackColor];
    [self.mailLabel setText:@"Mail: "];
    
    [self.view addSubview:self.mailLabel];
}

- (void)setupMailTextField {
    self.mailTextField = [[UITextField alloc] initWithFrame:CGRectMake(self.passwordTextField.frame.origin.x, self.mailLabel.frame.origin.y, self.passwordTextField.frame.size.width, self.passwordTextField.frame.size.height)];
    
    [self configureView:self.mailTextField];
    self.mailTextField.textAlignment = NSTextAlignmentCenter;
    self.mailTextField.placeholder = @" Mail";
    
    [self.view addSubview:self.mailTextField];
}

- (void)setupPlayerLabel {
    self.playerLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.mailLabel.frame.origin.x, CGRectGetMaxY(self.mailLabel.frame) + 20.0, self.mailLabel.frame.size.width, self.mailLabel.frame.size.height)];
    
    [self.playerLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.playerLabel setTextColor:kBlackColor];
    [self.playerLabel setText:@"Player"];
    
    [self.view addSubview:self.playerLabel];
}

- (void)setupPlayerSwitch {
    self.playerSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.mailTextField.frame.origin.x + (CGRectGetMaxX(self.mailTextField.frame) - self.mailTextField.frame.origin.x) / 2 - 25.0, CGRectGetMaxY(self.mailTextField.frame) + (CGRectGetMaxY(self.playerLabel.frame) - self.playerLabel.frame.origin.y) / 2, 50.0, 30.0)];
    
    [self.view addSubview:self.playerSwitch];
}

- (void)setupSubmitButton {
    self.submitButton = [[UIButton alloc] initWithFrame:CGRectMake(self.mailTextField.frame.origin.x, CGRectGetMaxY(self.playerLabel.frame) + 20.0, self.mailTextField.frame.size.width, self.mailTextField.frame.size.height)];
    
    [self configureView:self.submitButton];
    [self.submitButton setBackgroundColor:kBlackColor];
    [self.submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    [self.submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.submitButton addTarget:self action:@selector(submitButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.submitButton];
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)submitButtonPressed:(UIButton *)sender {
    AddUserResult result = AddUserResultSuccessful;
    
    if ([self.usernameTextField.text isEqualToString:@""]) {
        [self showAlertWithTitle:@"Error!" andMessage:@"Username can not be empty!"];
        result = AddUserResultFailed;
    
    } else if ([self.passwordTextField.text isEqualToString:@""]) {
        [self showAlertWithTitle:@"Error!" andMessage:@"Password can not be empty!"];
        result = AddUserResultFailed;
    
    } else if ([self.mailTextField.text isEqualToString:@""]) {
        [self showAlertWithTitle:@"Error!" andMessage:@"An e-mail address must be given!"];
        result = AddUserResultFailed;
    }
    
    if ([UserManagerInstance usernameExists:self.usernameTextField.text]) {
        [self showAlertWithTitle:@"Error!" andMessage:@"Chosen username already exists! Choose another!"];
        self.usernameTextField.text = @"";
        result = AddUserResultFailed;
    }
    
    if (result == AddUserResultSuccessful) {
    
        User *user = [[User alloc] init];
        user.userName = self.usernameTextField.text;
        user.password = self.passwordTextField.text;
        user.mail = self.mailTextField.text;
        user.player = self.playerSwitch.on;
        
        if (![UserManagerInstance addUser:user]) {
            [self showAlertWithTitle:@"Error" andMessage:@"Chosen username already exists! Try using another one."];
        }
    }
    
    self.usernameTextField.text = @"";
    self.passwordTextField.text = @"";
    self.mailTextField.text = @"";
    [self.playerSwitch setOn:NO];
    
    [self.view endEditing:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(addUserViewControllerDidTryToAddUserWithResult:)]) {
        [self.delegate addUserViewControllerDidTryToAddUserWithResult:result];
    }
}

@end
