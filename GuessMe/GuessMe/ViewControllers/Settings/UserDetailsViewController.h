//
//  UserDetailsViewController.h
//  GuessMe
//
//  Created by Daniel Ilisei on 11/11/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "User.h"

@interface UserDetailsViewController : UIViewController

@property (nonatomic, strong) User      *user;

/** Initializes a new isntance of the class with a given user.
 */
- (instancetype)initWithUser:(User *)user;

@end
