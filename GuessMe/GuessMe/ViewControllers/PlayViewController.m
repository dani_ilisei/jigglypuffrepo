//
//  PlayViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/22/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "PlayViewController.h"

#import "Utils.h"
#import "UserManager.h"

#import "BackButton.h"

#define kScoreLabelWidth 32.0
#define kButtonBackgroundColor [[UIColor lightGrayColor] colorWithAlphaComponent: 0.5]

@interface PlayViewController ()

@property (nonatomic, strong) BackButton        *backButton;

@property (nonatomic, strong) UILabel           *usernameLabel;
@property (nonatomic, strong) UILabel           *scoreDescriptiveLabel;
@property (nonatomic, strong) UILabel           *scoreLabel;

@property (nonatomic, strong) UILabel           *hintLabel;
@property (nonatomic, strong) UILabel           *scrambledWordLabel;
@property (nonatomic, strong) UITextField       *guessTextField;
@property (nonatomic, strong) UIButton          *submitButton;

@property (strong, nonatomic) NSArray           *fileContent;
@property (strong, nonatomic) NSArray           *fileLineContents;

@end

@implementation PlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
    [self addTapGesture];
    [self readFromFile];
    
    [self play];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setters

- (void)setUsername:(NSString *)username {
    _username = username;
    
    self.usernameLabel.text = username;
}

- (void)setScore:(NSString *)score {
    _score = score;
    
    self.scoreLabel.text = score;
}

#pragma mark - Private methods

- (void)addTapGesture {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    tapGesture.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tapGesture];
}

-(void) readFromFile{
    self.fileContent = [self customStringFromFile];
}

//Reads from text file.
-(NSArray*)customStringFromFile{
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"words" ofType:@"txt"];
    NSString* stringContent = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSArray* filecontent = [stringContent componentsSeparatedByString:@"\n"];
    return filecontent;
}

//Takes a random word from file content array.
-(NSArray*)fileLineContent: (NSArray*)fileContent{
    int randomIntgr = arc4random() % ([fileContent count] - 1);
    NSString* fileLine = fileContent[randomIntgr];
    
    NSArray* fileLineContent = [fileLine componentsSeparatedByString:@":"]; //fileLine[0]=word, fileLine[1]=points, fileLine[2]=hint
    
    return fileLineContent;
}

//Shuffles the letters of the received word.
-(NSString *)scrambleString:(NSString *)toScramble {
    for (int i = 0; i < [toScramble length] * 15; i ++) {
        int pos = arc4random() % [toScramble length];
        int pos2 = arc4random() % ([toScramble length] - 1);
        char ch = [toScramble characterAtIndex:pos];
        if (strcmp(&ch, " ") != 0) {
            NSString *before = [toScramble substringToIndex:pos];
            NSString *after = [toScramble substringFromIndex:pos + 1];
            NSString *temp = [before stringByAppendingString:after];
            before = [temp substringToIndex:pos2];
            after = [temp substringFromIndex:pos2];
            toScramble = [before stringByAppendingFormat:@"%c%@", ch, after];
        }
    }
    return toScramble;
}


- (void)play {
    self.fileLineContents = [self fileLineContent:self.fileContent];
    NSString *scrambledWord = [self scrambleString:[self.fileLineContents objectAtIndex: 0]];
    
    self.scrambledWordLabel.text = scrambledWord;
    
    if (self.fileLineContents.count == 3) {
        self.hintLabel.text = [NSString stringWithFormat:@"Hint: %@",[self.fileLineContents objectAtIndex:2]];
    }
}


- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message {
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:alertC completion:nil];
    }];
    
    [alertC addAction:action];
    
    [self presentViewController:alertC animated:YES completion:nil];
}

#pragma mark - UI & action methods

- (void)setupUI {
    [self setupView];
    
    [self setupBackButton];
    
    [self setupUsernameLabel];
    [self setupScoreLabel];
    [self setupScoreDesciptiveLabel];
    
    [self setupHintLabel];
    [self setupScrambledWordLabel];
    [self setupGuessTextField];
    [self setupSubmitButton];
}

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setupBackButton {
    self.backButton = [[BackButton alloc] initWithFrame:CGRectMake(kBackButtonWidth / 2, kBackButtonWidth, kBackButtonWidth, kBackButtonWidth)];
    
    [self.backButton addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.backButton];
}

- (void)setupUsernameLabel {
    self.usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - kDefaultMargin - kGenericElementWidth, kDefaultTopMargin, kGenericElementWidth, kGenericElementHeight)];
    
    self.usernameLabel.textAlignment = NSTextAlignmentRight;
    self.usernameLabel.textColor = kBlackColor;
    self.usernameLabel.text = UserManagerInstance.currentLoggedUser.userName;
    
    [self.view addSubview:self.usernameLabel];
}

- (void)setupScoreLabel {
    self.scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width - kDefaultMargin - kScoreLabelWidth, CGRectGetMaxY(self.usernameLabel.frame), kScoreLabelWidth, kGenericElementHeight)];
    
    self.scoreLabel.textAlignment = NSTextAlignmentRight;
    self.scoreLabel.textColor = kBlackColor;
    self.score = @"0";
    
    [self.view addSubview:self.scoreLabel];
}

- (void)setupScoreDesciptiveLabel {
    self.scoreDescriptiveLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.scoreLabel.frame) - kGenericElementWidth, self.scoreLabel.frame.origin.y, kGenericElementWidth, kGenericElementHeight)];
    
    self.scoreDescriptiveLabel.textAlignment = NSTextAlignmentRight;
    self.scoreDescriptiveLabel.textColor = kBlackColor;
    self.scoreDescriptiveLabel.text = @"Score: ";
    
    [self.view addSubview:self.scoreDescriptiveLabel];
}

- (void)setupHintLabel {
    self.hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 4, self.view.frame.size.height / 3, self.view.frame.size.width / 2, kGenericElementHeight)];
    
    self.hintLabel.textAlignment = NSTextAlignmentCenter;
    self.hintLabel.textColor = kBlackColor;
    self.hintLabel.text = @"Hint: Some hint ..";
    self.hintLabel.adjustsFontSizeToFitWidth = YES;
    
    [self.view addSubview:self.hintLabel];
}

- (void)setupScrambledWordLabel {
    self.scrambledWordLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.hintLabel.frame.origin.x, CGRectGetMaxY(self.hintLabel.frame), self.hintLabel.frame.size.width, self.hintLabel.frame.size.height)];
    
    self.scrambledWordLabel.textAlignment = NSTextAlignmentCenter;
    self.scrambledWordLabel.textColor = kBlackColor;
    self.scrambledWordLabel.text = @"Scrambled word here.";
    
    [self.view addSubview:self.scrambledWordLabel];
}

- (void)setupGuessTextField {
    self.guessTextField = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 6, CGRectGetMaxY(self.scrambledWordLabel.frame) + 20.0, 2 * self.view.frame.size.width / 3, kGenericElementHeight)];
    
    self.guessTextField.placeholder = @"Guess me ...";
    self.guessTextField.textAlignment = NSTextAlignmentCenter;
    
    self.guessTextField.layer.borderWidth = kBorderWidth;
    self.guessTextField.layer.borderColor = kBlackColor.CGColor;
    self.guessTextField.layer.cornerRadius = kCornerRadius;
    
    [self.view addSubview:self.guessTextField];
}

- (void)setupSubmitButton {
    self.submitButton = [[UIButton alloc] initWithFrame:CGRectMake(self.hintLabel.frame.origin.x, CGRectGetMaxY(self.guessTextField.frame) + 20.0, self.hintLabel.frame.size.width, kGenericElementHeight)];
    
    [self.submitButton setTitle:@"Sumbit guess" forState:UIControlStateNormal];
    [self.submitButton setTitleColor:kBlackColor forState:UIControlStateNormal];
    [self.submitButton setBackgroundColor:kButtonBackgroundColor];
    
    self.submitButton.layer.borderWidth = kBorderWidth;
    self.submitButton.layer.cornerRadius = kCornerRadius;
    self.submitButton.layer.borderColor = kBlackColor.CGColor;
    
    [self.submitButton addTarget:self action:@selector(submitButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.submitButton];
}

- (void)submitButtonPressed:(UIButton *)sender {
    if ([self.guessTextField.text isEqualToString:@""]) {
        [self showAlertWithTitle:@"Error!" andMessage:@"Answer can not be empty!\nTry to guess!"];
    
    } else {
        if ([[self.guessTextField.text lowercaseString] isEqualToString:[[self.fileLineContents objectAtIndex:0] lowercaseString]]) {
            self.score = [NSString stringWithFormat:@"%d",[self.score intValue] + [[self.fileLineContents objectAtIndex:1] intValue]];
        
        } else {
            self.score = [NSString stringWithFormat:@"%d",[self.score intValue] - 1];
        }
        
        self.guessTextField.text = @"";
        
        [self play];
    }
}

- (void)backButtonPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dismissKeyboard:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (!CGRectContainsPoint(CGRectMake(self.submitButton.frame.origin.x, self.submitButton.frame.origin.y, self.submitButton.frame.size.width, self.submitButton.frame.size.height),[tapGestureRecognizer locationInView:self.view] )){
        [self.view endEditing:YES];
    }
}

@end
