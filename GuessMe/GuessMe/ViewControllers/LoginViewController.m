//
//  LoginViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "LoginViewController.h"
#import "AddUserViewController.h"

#import "UserManager.h"

#import "Utils.h"

@interface LoginViewController () <AddUserViewControllerDelegate>

@property (nonatomic, strong) UIView                    *usernameContainer;
@property (nonatomic, strong) UIView                    *passwordContainer;

@property (nonatomic, strong) UILabel                   *usernameLabel;
@property (nonatomic, strong) UITextField               *usernameTextField;

@property (nonatomic, strong) UILabel                   *passwordLabel;
@property (nonatomic, strong) UITextField               *passwordTextField;

@property (nonatomic, strong) UIButton                  *okButton;
@property (nonatomic, strong) UIButton                  *registerButton;

@end

@implementation LoginViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods

- (void)showAlertWithTitle:(NSString *)title andBody:(NSString *)message {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:alertVC completion:nil];
    }];
                               
    [alertVC addAction:okAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

#pragma mark - AddUserViewControllerDelegate methods

- (void)addUserViewControllerDidTryToAddUserWithResult:(AddUserResult)result {
    if (result == AddUserResultSuccessful) {
        [self.navigationController popToViewController:self animated:NO];
        [self.navigationController popViewControllerAnimated:NO]; //Pop self.
    }
}

#pragma mark - UI & action methods

- (void)setupUI {

    [self setupView];
    [self setupUsernameLabel];
    [self setupPasswordLabel];
    
    [self setupUsernameField];
    [self setupPasswordField];
    
    [self setupOKButton];
    [self setupRegisterButton];
}

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)setupUsernameLabel {
    self.usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultLabelMargin, self.view.frame.size.height / 4, kLabelWidth, kContainerHeight)];
    [self.usernameLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.usernameLabel setTextColor:[UIColor blackColor]];
    [self.usernameLabel setText:@"Username: "];
    
    [self.view addSubview:self.usernameLabel];
}

- (void)setupPasswordLabel {
    self.passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.usernameLabel.frame.origin.x, CGRectGetMaxY(self.usernameLabel.frame) + 20.0, self.usernameLabel.frame.size.width, self.usernameLabel.frame.size.height)];
    [self.passwordLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.passwordLabel setTextColor:[UIColor blackColor]];
    [self.passwordLabel setText:@"Password: "];
    
    [self.view addSubview:self.passwordLabel];
}

- (void)setupUsernameField {
    self.usernameTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.usernameLabel.frame) + kDefaultMargin, self.usernameLabel.frame.origin.y, self.view.frame.size.width - 2 * kDefaultLabelMargin - kDefaultMargin - self.usernameLabel.frame.size.width, self.usernameLabel.frame.size.height)];
    self.usernameTextField.placeholder = @"  Enter username ..";
    self.usernameTextField.layer.cornerRadius = kCornerRadius;
    self.usernameTextField.layer.borderWidth = kBorderWidth;
    self.usernameTextField.layer.borderColor = [UIColor blackColor].CGColor;
    self.usernameTextField.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:self.usernameTextField];
}

- (void)setupPasswordField {
    self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.passwordLabel.frame) + kDefaultMargin, self.passwordLabel.frame.origin.y, self.view.frame.size.width - 2 * kDefaultLabelMargin - kDefaultMargin - self.passwordLabel.frame.size.width, self.passwordLabel.frame.size.height)];
    self.passwordTextField.placeholder = @"  Enter password ..";
    self.passwordTextField.layer.cornerRadius = kCornerRadius;
    self.passwordTextField.layer.borderWidth = kBorderWidth;
    self.passwordTextField.layer.borderColor = [UIColor blackColor].CGColor;
    self.passwordTextField.secureTextEntry = YES;
    self.passwordTextField.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:self.passwordTextField];
}

- (void)setupOKButton {
    self.okButton = [[UIButton alloc] initWithFrame:CGRectMake(self.passwordTextField.frame.origin.x, CGRectGetMaxY(self.passwordTextField.frame) + 20.0, self.passwordTextField.frame.size.width, self.passwordTextField.frame.size.height)];
    [self.okButton setBackgroundColor:[UIColor blackColor]];
    [self.okButton setTitle:@"Login" forState:UIControlStateNormal];
    [self.okButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.okButton.layer.cornerRadius = kCornerRadius;
    self.okButton.layer.borderWidth = kBorderWidth;
    self.okButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self.okButton addTarget:self action:@selector(OKButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.okButton];
}

- (void)setupRegisterButton {
    self.registerButton = [[UIButton alloc] initWithFrame:CGRectMake(self.okButton.frame.origin.x, CGRectGetMaxY(self.okButton.frame) + 20.0, self.okButton.frame.size.width, self.okButton.frame.size.height)];
    [self.registerButton setBackgroundColor:[UIColor blackColor]];
    [self.registerButton setTitle:@"Register" forState:UIControlStateNormal];
    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.registerButton.layer.cornerRadius = kCornerRadius;
    self.registerButton.layer.borderWidth = kBorderWidth;
    self.registerButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self.registerButton addTarget:self action:@selector(registerButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.registerButton];
}

- (void)OKButtonPressed:(UIButton *)sender {
    
    if (![self.usernameTextField.text isEqualToString:@""]) {
        [UserManagerInstance loginWithUsername:self.usernameTextField.text andPassword:self.passwordTextField.text];
        
        switch (UserManagerInstance.loginResult) {
            case UserLoginResultSuccessful:
                [self.navigationController popViewControllerAnimated:YES];
                break;
                
            case UserLoginResultUsernameNotFound:
                [self showAlertWithTitle:@"Error" andBody:@"Username not found. Try again!"];
                break;
                
            case UserLoginResultWrongPassword:
                [self showAlertWithTitle:@"Error" andBody:@"Wrong password. Try again!"];
                break;
                
            default:
                break;
        }
    
    } else {
        [self showAlertWithTitle:@"Error" andBody:@"Username cannot be emtpy! Try again!"];
    }
}

- (void)registerButtonPressed:(UIButton *)sender {
    AddUserViewController *addUserViewController = [[AddUserViewController alloc] init];
    addUserViewController.delegate = self;
    [self.navigationController pushViewController:addUserViewController animated:YES];
}
@end
