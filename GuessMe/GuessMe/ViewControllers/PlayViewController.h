//
//  PlayViewController.h
//  GuessMe
//
//  Created by Daniel Ilisei on 11/22/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayViewController : UIViewController

@property (nonatomic, strong) NSString          *username;
@property (nonatomic, strong) NSString          *score;

@end
