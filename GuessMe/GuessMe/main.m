//
//  main.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
