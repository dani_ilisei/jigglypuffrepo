//
//  ViewController.m
//  GuessMe
//
//  Created by Daniel Ilisei on 11/9/16.
//  Copyright © 2016 Daniel Ilisei. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
#import "UserDetailsViewController.h"
#import "SettingsViewController.h"
#import "PlayViewController.h"

#import "UserManager.h"
#import "User.h"
#import "Utils.h"

#import "UserCellTableViewCell.h"

@interface ViewController ()

@property (nonatomic, strong) UIButton          *settingsButton;
@property (nonatomic, strong) UILabel           *userRoleLabel;
@property (nonatomic, strong) UIButton          *playButton;

@end

@implementation ViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setupUI];
    [self addObservers];
    
    LoginViewController *loginVC = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:loginVC animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - Private methods
    
- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeLoggedUser:) name:kDidChangeLoggedUserNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveLocalNotification:) name:kDidReceiveLocalNotification object:nil];
}
    
- (void)didChangeLoggedUser:(NSNotification *)notification {
    NSString *userRole = @"Challenger";
    if ([UserManagerInstance currentLoggedUser].player) {
        userRole = @"Player";
    }
    
    [self.userRoleLabel setText:userRole];
}
    
- (void)didReceiveLocalNotification:(NSNotification *)notification {
    UILocalNotification *localNoti = [notification.userInfo objectForKey:@"notification"];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:localNoti.alertTitle message:localNoti.alertBody preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UI & action methods

- (void)setupUI {
    
    [self setupView];
    [self setupUserRoleLabel];
    [self setupSettingsButton];
    [self setupPlayButton];
}

- (void)setupView {
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.navigationController.navigationBarHidden = YES;
}

- (void)setupUserRoleLabel {
    self.userRoleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultMargin, kDefaultTopMargin, kGenericElementWidth, kGenericElementHeight)];
    [self.userRoleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:kFontSize]];
    [self.userRoleLabel setTextColor:kBlackColor];
    
    [self.view addSubview:self.userRoleLabel];
}

- (void)setupSettingsButton {
    self.settingsButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - kDefaultMargin - kGenericElementWidth, kDefaultTopMargin, kGenericElementWidth, kGenericElementHeight)];
    [self.settingsButton setImage:[UIImage imageNamed:@"options_icon"] forState:UIControlStateNormal];
    
    [self.settingsButton setTitle:@"Settings" forState:UIControlStateNormal];
    [self.settingsButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.settingsButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    self.settingsButton.tintColor = [UIColor lightGrayColor];
    self.settingsButton.layer.borderWidth = kBorderWidth;
    self.settingsButton.layer.cornerRadius = kCornerRadius;
    self.settingsButton.layer.borderColor = kBorderColor;
    
    [self.settingsButton addTarget:self action:@selector(settingsButtonsPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.settingsButton];
}

- (void)setupPlayButton {
    self.playButton =  [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width - kGenericElementWidth) / 2, self.view.frame.size.height / 2, kGenericElementWidth, kGenericElementHeight)];
    
    [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
    [self.playButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.playButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    self.playButton.layer.borderWidth = kBorderWidth;
    self.playButton.layer.cornerRadius = kCornerRadius;
    self.playButton.layer.borderColor = kBorderColor;
    
    [self.playButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.playButton];
}

- (void)settingsButtonsPressed:(UIButton *)sender {
//    UsersTableViewController *usersTableViewController = [[UsersTableViewController alloc] init];
//    
//    [self.navigationController pushViewController:usersTableViewController animated:YES];
    
    SettingsViewController *settingsViewController = [[SettingsViewController alloc] init];
    
    [self.navigationController pushViewController:settingsViewController animated:YES];
}

- (void)playButtonPressed:(UIButton *)sender {
    PlayViewController *playViewController = [[PlayViewController alloc] init];
    playViewController.username = @"Dani";
    
    [self.navigationController pushViewController:playViewController animated:YES];
}

@end
