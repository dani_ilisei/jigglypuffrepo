/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    AlertIOS,
    StyleSheet,
    Text,
    View,
    Navigator,
    ListView,
    TextInput,
    StatusBar,
    TouchableHighLight,
    TouchableNativeFeedback,
    TouchableOpacity,
    AsyncStorage,
    NavigatorIOS,
} from 'react-native';

import Chart from 'react-native-chart';
import Communications from 'react-native-communications';
import Button from 'react-native-button';

var Mailer = require('NativeModules').RNMail

var users = []

const data = [[
               [3, 7],
               [4, 9],
               ]];

class User extends Component {
    constructor(props) {
        super(props);
        this.state = (username: "",password: "",mail: "", role: "");
        this.onPress = this.onPress.bind(this);

    }
}

var AppDelegate = React.createClass({
                                render () {
                                return (
                                        <NavigatorIOS ref="nav" style={{flex: 1}} initialRoute={{
                                        component: ReactNativeGuessMe,
                                        title: 'GuessMe',
                                        }} />
                                        );
                                }
                                });

export default class ReactNativeGuessMe extends Component {
    constructor(props) {
        super(props);
        
        if (users.length == 0) {
            this._getPersistedData();
        }
        
        this.state = {
        name: '',
        password: '',
        mail: '',
        role: '',
        dataSource: new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2,
            }),
        loaded: false,
        };
    }
    
    componentDidMount() {
        this._getPersistedData();
        
        this.setState({
                      dataSource: this.state.dataSource.cloneWithRows(users),
                      loaded: true,
                      });
    }
    
    _persistData() {
        return AsyncStorage.setItem('key1', JSON.stringify(users))
        .then(json => console.log('success at persist save'))
        .catch(error => console.log('error at persist save'));
    }
    
    _getPersistedData() {
        return AsyncStorage.getItem('key1')
        .then(req => JSON.parse(req))
        .then(json => {
              
              console.log(json)
              users.splice(0, users.length);
              
              for (var i = 0; i < json.length; i++) {
              users.push({
                            "username": json[i].username,
                            "password": json[i].password,
                            "mail": json[i].mail,
                            "role": json[i].role
                            });
              
              
              this.setState({
                            dataSource: this.state.dataSource.cloneWithRows(users),
                            loaded: true,
                            });
              }
              })
        .catch(error => console.log('error at reading!'));
    }
    
    _addBtn(){
        if (this.state.username !== '' && this.state.password != '' && this.state.mail != '' && this.state.role != '') {
            users.push({"username": this.state.username, "password": this.state.password, "mail": this.state.mail, "role": this.state.role});
//            AlertIOS.alert("Done", "User added!");
            this.setState({
                          dataSource: this.state.dataSource.cloneWithRows(users),
                          loaded: true,
                          });
                        this._persistData();
        } else {
//            AlertIOS.alert("Warning", "Some input(s) are empty!");
        }
    }
    
    _emailBtn() {
        
        
        Mailer.mail({
                    subject: 'ReactNativeMail',
                    recipients: ['dani.ilisei@gmail.com'],
                    ccRecipients: ['supportCC@example.com'],
                    bccRecipients: ['supportBCC@example.com'],
                    body: 'Mail sent via ReactNativeGuessMe',
                    isHTML: true, // iOS only, exclude if false
                    attachment: {
                    path: 'somePath',  // The absolute path of the file from which to read data.
                    type: 'jpg',   // Mime Type: jpg, png, doc, ppt, html, pdf
                    name: 'someName',   // Optional: Custom filename for attachment
                    }
                    }, (error, event) => {
                    if(error) {
//                    AlertIOS.alert('Error', 'Could not send mail. Please send a mail to support@example.com');
                    }
                    });
    }
    
    _navigate(user){

        this.props.navigator.push({
                                  title: 'Edit Details',
                                  component:EditDetails,
                                  passProps: {
                                  user : user
                                  }
        });
    }
    
    _seeChart() {
        this.props.navigator.push({
                                  title: 'github.com/tomauty',
                                  component:SimpleChart,
                                  });
    }
    
    renderUsers(users) {
        return (
                <TouchableOpacity
                onPress={ () => this._navigate(users)}>
                <View
                style={styles.viewDetails}>
                <Text>{users.username}</Text>
                <Text>{users.mail}</Text>
                <Text>{users.role}</Text>
                
                </View>
                </TouchableOpacity>
                );
    }
    
  render() {
    return (
            
            <View style={{backgroundColor: 'transparent'}}>
            
            <Text style={styles.header}> Welcome to GuessME</Text>
            
            <TextInput
            style= {styles.input}
            onChangeText={(text) => this.setState({username : text})}
            placeholder="Username"
            value = {this.state.username}/>
            
            <TextInput
            style= {styles.input}
            onChangeText={(text) => this.setState({password : text})}
            placeholder="Password"
            value = {this.state.password}/>
        
            <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({mail : text})}
            placeholder="Mail"
            value = {this.state.mail}/>
        
            <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({role : text})}
            placeholder="Role"
            value = {this.state.role}/>
            
            <Button
            containerStyle={{padding:8, height:50, overflow:'hidden', borderRadius:6, backgroundColor: '#ff9900', marginBottom: 4}}
            style={{fontSize: 20, color: 'white'}}
            styleDisabled={{color: '#1565C0'}}
            onPress={() => this._addBtn()}>Add</Button>
            
            <Button
            containerStyle={{padding:8, height:50, overflow:'hidden', borderRadius:6, backgroundColor: '#009900'}}
            style={{fontSize: 20, color: 'white'}}
            styleDisabled={{color: '#1565C0'}}
            onPress={() => this._emailBtn()}>Send email</Button>
            
            <ListView
            dataSource = {this.state.dataSource}
            renderRow = {this.renderUsers.bind(this)}
            style = {styles.listView}/>
            
            <Button
            containerStyle={{padding:8, height:50, overflow:'hidden', borderRadius:6, backgroundColor: '#666633'}}
            style={{fontSize: 20, color: 'white'}}
            styleDisabled={{color: '#1565C0'}}
            onPress={() => this._seeChart()}>See Chart</Button>
            
            </View>
      )
  }
}

class SimpleChart extends Component {
    render() {
        return (
                <View style={styles.containerChart}>
                
                <Chart
                style={styles.chart}
                data={data}
                verticalGridStep={5}
                type="pie"
                sliceColors={['#ff9900', '#009900']}
                />
                </View>
                );
    }
}

class EditDetails extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
        username : this.props.user.username ,
        password : this.props.user.password,
        mail: this.props.user.mail,
        role: this.props.user.role,
        }
        
    }
    
    _persistData() {
        return AsyncStorage.setItem('key1', JSON.stringify(users))
        .then(json => console.log('success at persist save'))
        .catch(error => console.log('error at persist save'));
    }
    
    
    /**
     * when pressing the Save button
     */
    _handlePress() {
        if (this.state.username !== '' && this.state.password!== '' && this.state.mail != '' && this.state.role != '') {
            this.props.user.username = this.state.username;
            this.props.user.password = this.state.password;
            this.props.user.mail = this.state.mail;
            this.props.user.role = this.state.role;
//            AlertIOS.alert("Saved");
            
            this._persistData();
            
            
        } else {
//            AlertIOS.alert("Warning", "One or more fields are empty!");
        }
    }
    
    _handlePressDelete() {
        var index = users.indexOf(this.props.user);
        
        if (index > -1) {
            
            users.splice(index, 1);
//            AlertIOS.alert("Done", "Deleted.");
            
            this._persistData();
            
            this.props.navigator.pop();
        }
        else {
//            AlertIOS.alert("Warning", "User not found");
        }
    }
    
    render(){
        return(
               <View style={{backgroundColor: 'white'}}>
               <Text style={styles.header}>Edit</Text>
               
               <TextInput
               style= {styles.input}
               onChangeText={(text) => this.setState({username : text})}
               placeholder="Username"
               value = {this.state.username}
               />
               <TextInput
               style = {styles.input}
               onChangeText = {(text) => this.setState({password : text})}
               placeholder = "Password"
               value = {this.state.password}
               />
               <TextInput
               style={styles.input}
               onChangeText={(text) => this.setState({mail : text})}
               placeholder="Mail"
               value = {this.state.mail}
               />
               <TextInput
               style={styles.input}
               onChangeText={(text) => this.setState({role : text})}
               placeholder="Role"
               value = {this.state.role}
               />
               
               <Button
               containerStyle={{padding:10, height:45, overflow:'hidden', borderRadius:4, backgroundColor: 'lightgrey', marginBottom: 4}}
               style={{fontSize: 20, color: 'black'}}
               styleDisabled={{color: 'red'}}
               onPress={ () => this._handlePress() }>
               Save
               </Button>
               
               <Button
               containerStyle={{
               padding: 10,
               height: 45,
               overflow: 'hidden',
               borderRadius: 4,
               backgroundColor: 'red',
               marginBottom: 4
               }}
               style={{fontSize: 20, color: 'white'}}
               styleDisabled={{color: 'red'}}
               onPress={ () => this._handlePressDelete() }>
               Delete
               </Button>
               </View>
               )
    }
    
}



const styles = StyleSheet.create({
                                 
  header: {
     fontSize: 24,
     textAlign: 'center',
     marginTop: 40,
     marginBottom: 10,
     color: '#ACEAE8C0',
  },
  input: {
     height: 40,
     borderColor: 'gray',
     borderWidth: 1,
     marginBottom: 5,
  },
  listView: {
     width: 320,
     paddingTop: 1,
     backgroundColor: 'transparent',
                                 
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
 containerChart: {
 flex: 1,
 justifyContent: 'center',
 alignItems: 'center',
 backgroundColor: 'white',
 },
 chart: {
 marginTop: 0,
 width: 300,
 height: 300,
 },
});

AppRegistry.registerComponent('ReactNativeGuessMe', () => AppDelegate);
