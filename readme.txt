App Story

GuessMe is a guessing game.
An app in there are 2 categories of users: players and challengers.
The players receive hints about a specific word and they have to guess it.
The challengers set challenges for players, to make them choose specific categories from which to guess.

1. User can authenticate (as challenger or player)
2. User can chose categories about words to be guessed.
3. Top 5 highscores for players.
4. Statistics about how many times each player played the game.

